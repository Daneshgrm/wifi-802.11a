`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:50:31 06/30/2020
// Design Name:   encoder
// Module Name:   C:/.Xilinx/encoder/encoder_tb.v
// Project Name:  encoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: encoder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module encoder_tb;

	// Inputs
	reg clk;
	reg rate;
	reg in;
	reg request;
   reg [5:0]temp_res;
	reg [1:0]counter=2'b0;
	reg [9:0]failures=0;
	// Outputs
	wire [5:0] out;

	// Instantiate the Unit Under Test (UUT)
	encoder uut (
		.clk(clk), 
		.rate(rate), 
		.in(in), 
		.request(request), 
		.out(out)
	);
  integer op1,temp1,op2,temp2;
	initial begin
		// Initialize Inputs
		clk = 0;
		rate = 0;
		in = 0;
		request = 1;
		op1=$fopen("in.txt","r"); 
		op2=$fopen("out.txt","r"); 
		 temp1= $fscanf (op1, "%b \n", in);
end
		// Wait 100 ns for global reset to finish
	
        always  #5 clk<= ~clk ;
		// Add stimulus here
always@(posedge clk) 
					begin
					   temp1= $fscanf (op1, "%b \n", in);
						
						counter=counter+1;
						if(counter==3)
						begin
						
						if(rate==1)
						begin
						temp1= $fscanf (op2, "%b \n", temp_res[5]);
						temp1= $fscanf (op2, "%b \n", temp_res[4]);
						temp1= $fscanf (op2, "%b \n", temp_res[3]);
						temp1= $fscanf (op2, "%b \n", temp_res[2]);
						end
						if(rate==0)
						begin
						temp1= $fscanf (op2, "%b \n", temp_res[5]);
						temp1= $fscanf (op2, "%b \n", temp_res[4]);
						temp1= $fscanf (op2, "%b \n", temp_res[3]);
						temp1= $fscanf (op2, "%b \n", temp_res[2]);
						temp1= $fscanf (op2, "%b \n", temp_res[1]);
						temp1= $fscanf (op2, "%b \n", temp_res[0]);
						end
						if(counter==0)
						begin
						if(rate==1 && temp_res[5:2]!=out[5:2])
						failures=failures+1;
                  if(rate==0 && temp_res[5:0]!=out[5:0])
						failures=failures+1;
						end
						counter=0;
      end
		end
endmodule

