module encoder(
	input wire clk,
	input wire rate,
	input wire in,
	input wire request,
	output reg [5:0] out
    );
	
	reg [6:0] window=7'b0;
	wire A, B;
	reg [1:0] counter=2'b0;
	assign A = window[0] ^ window[2] ^ window[3] ^ window[5] ^ window[6];
	assign B = window[0] ^ window[1] ^ window[2] ^ window[3] ^ window[6];
	
	always @(posedge clk ) 
	begin
 if (request) begin
			window[0] <= in;
			window[6:1] <= window[5:0];
			counter <= counter + 1'b1;
		end
	
		case (counter)
			2'd1: out[5:4] <= {A,B};
			2'd2: out[3:2] <= {A,B};
			2'd3:
				begin
					if (rate)
               begin					
					out[2] <= B;
					out[1:0]<=2'b00;
					end
					else 
					out[1:0] <= {A,B}; 
					counter <= request ? 2'd1 : 2'd0;
				end
		endcase
	end					
 endmodule
 