module decoder(
	input clk,
	input resetn,
	input rx,
	input request,
	input rate,
	output reg done,
	output reg [35:0] dx
    );
	
	reg [4:0]cost[0:63];
	reg trace[0:63][0:35];	
	reg pre_trace[0:63][0:35];
	
	reg [1:0]data;
	reg [1:0]counter;
	reg ready; //reading data
	integer i, j;
	
	reg [5:0] step_counter;
	
	wire [1:0] hamming00, hamming01, hamming10, hamming11;
	
	assign hamming00 = (rate & (~^counter)) ? ( &counter ? {1'b0,data[1]}  : {1'b0,data[0]} )  : {1'b0,data[1]}  + {1'b0,data[0]};
	assign hamming01 = (rate & (~^counter)) ? ( &counter ? {1'b0,data[1]}  : {1'b0,~data[0]} ) : {1'b0,data[1]}  + {1'b0,~data[0]};
	assign hamming10 = (rate & (~^counter)) ? ( &counter ? {1'b0,~data[1]} : {1'b0,data[0]} )  : {1'b0,~data[1]} +  {1'b0,data[0]};
	assign hamming11 = (rate & (~^counter)) ? ( &counter ? {1'b0,~data[1]} : {1'b0,~data[0]} ) : {1'b0,~data[1]} + {1'b0,~data[0]};
	
	reg start_compare;
	wire done_compare;
	wire [5:0] min_cost_loc;
	
	comparator64 comparator (.clk(clk), .resetn(resetn), .start(start_compare), .done(done_compare), .num_res0(min_cost_loc),
								.in0(cost[0]), .in1(cost[1]), .in2(cost[2]), .in3(cost[3]), .in4(cost[4]), .in5(cost[5]), .in6(cost[6]), .in7(cost[7]), .in8(cost[8]), 
								.in9(cost[9]), .in10(cost[10]), .in11(cost[11]), .in12(cost[12]), .in13(cost[13]), .in14(cost[14]), .in15(cost[15]), .in16(cost[16]), 
								.in17(cost[17]), .in18(cost[18]), .in19(cost[19]), .in20(cost[20]), .in21(cost[21]), .in22(cost[22]), .in23(cost[23]), .in24(cost[24]), 
								.in25(cost[25]), .in26(cost[26]), .in27(cost[27]), .in28(cost[28]), .in29(cost[29]), .in30(cost[30]), .in31(cost[31]), .in32(cost[32]), 
								.in33(cost[33]), .in34(cost[34]), .in35(cost[35]), .in36(cost[36]), .in37(cost[37]), .in38(cost[38]), .in39(cost[39]), .in40(cost[40]), 
								.in41(cost[41]), .in42(cost[42]), .in43(cost[43]), .in44(cost[44]), .in45(cost[45]), .in46(cost[46]), .in47(cost[47]), .in48(cost[48]), 
								.in49(cost[49]), .in50(cost[50]), .in51(cost[51]), .in52(cost[52]), .in53(cost[53]), .in54(cost[54]), .in55(cost[55]), .in56(cost[56]), 
								.in57(cost[57]), .in58(cost[58]), .in59(cost[59]), .in60(cost[60]), .in61(cost[61]), .in62(cost[62]), .in63(cost[63]));

	reg decode_flag;
	reg [5:0] location;
	reg [5:0] decode_counter;
	reg request_flag;

	always @(posedge clk or negedge resetn) begin
	
		if (~resetn) begin
			done <= 1'b0;
			decode_flag <= 1'b0;
			start_compare <= 1'b0;
			counter <= 2'd0; 
			cost[0] <= 5'd0;
			for (i = 1; i < 64; i = i + 1) begin
				cost[i] <= 5'd20;
			end
			ready <= 1'b0;
			step_counter <= 6'd0;
			request_flag <= 1'b0;
		end
		
		else begin
		
			if (request) begin
				request_flag <= 1'b1;
				counter <= counter + 1'b1;
				ready <= 1'b0;
				case (counter)
					2'd0: data[1] <= rx;
					2'd1: 
						begin
							data[0] <= rx;
							ready <= 1'b1;
						end
					2'd2: 
						begin
							if (rate) begin
								data <= {rx,1'b0};
								ready <= 1'b1;
							end
							
							else data[1] <= rx;
						end
					2'd3:
						begin
							ready <= 1'b1;
							if (rate) data <= {1'b0,rx};
							else data[0] <= rx;
						end
				endcase
			end
			
			else if (request_flag) begin
				start_compare <= 1'b1;
				decode_counter <= step_counter;
				request_flag <= 1'b0;
			end
				
			
			if (start_compare) begin //decoding
				start_compare <= 1'b0;
				for (i = 0; i < 64; i = i + 1) begin
					for (j = 0; j < 36; j = j + 1) begin
						pre_trace[i][j] <= trace[i][j];
					end
				end
				decode_counter <= request_flag ? 6'd35 : decode_counter;
			end
					
			if (done_compare) begin
				dx[35 - decode_counter] <= min_cost_loc[0];
				location <= pre_trace[min_cost_loc][decode_counter] ? {1'b1, min_cost_loc[5:1]} : {1'b0, min_cost_loc[5:1]};
				decode_counter <= decode_counter - 1'b1;
				decode_flag <= 1'b1;
			end
			
			if (decode_flag) begin
				dx[35 - decode_counter] <= location[0];
				location <= pre_trace[location][decode_counter] ? {1'b1, location[5:1]} : {1'b0, location[5:1]};
				decode_counter <= decode_counter - 1'b1;
				{decode_flag, done} <= (decode_counter == 6'd0) ? {1'b0, 1'b1} : {1'b1, 1'b0};
			end
			
			if (done) done <= 1'b0;
			
			if (ready) begin
				{step_counter, start_compare} <= (step_counter < 6'd35) ? {(step_counter + 1'b1), 1'b0} : {6'd0, 1'b1};
				{cost[0], trace[0][step_counter]}  <= (cost00  < cost01)  ? {cost00,1'b0}  : {cost01,1'b1};
				{cost[1], trace[1][step_counter]}  <= (cost10  < cost11)  ? {cost10,1'b0}  : {cost11,1'b1};
				{cost[2], trace[2][step_counter]}  <= (cost20  < cost21)  ? {cost20,1'b0}  : {cost21,1'b1};
				{cost[3], trace[3][step_counter]}  <= (cost30  < cost31)  ? {cost30,1'b0}  : {cost31,1'b1};
				{cost[4], trace[4][step_counter]}  <= (cost40  < cost41)  ? {cost40,1'b0}  : {cost41,1'b1};
				{cost[5], trace[5][step_counter]}  <= (cost50  < cost51)  ? {cost50,1'b0}  : {cost51,1'b1};
				{cost[6], trace[6][step_counter]}  <= (cost60  < cost61)  ? {cost60,1'b0}  : {cost61,1'b1};
				{cost[7], trace[7][step_counter]}  <= (cost70  < cost71)  ? {cost70,1'b0}  : {cost71,1'b1};
				{cost[8], trace[8][step_counter]}  <= (cost80  < cost81)  ? {cost80,1'b0}  : {cost81,1'b1};
				{cost[9], trace[9][step_counter]}  <= (cost90  < cost91)  ? {cost90,1'b0}  : {cost91,1'b1};
				{cost[10],trace[10][step_counter]} <= (cost100 < cost101) ? {cost100,1'b0} : {cost101,1'b1};
				{cost[11],trace[11][step_counter]} <= (cost110 < cost111) ? {cost110,1'b0} : {cost111,1'b1};
				{cost[12],trace[12][step_counter]} <= (cost120 < cost121) ? {cost120,1'b0} : {cost121,1'b1};
				{cost[13],trace[13][step_counter]} <= (cost130 < cost131) ? {cost130,1'b0} : {cost131,1'b1};
				{cost[14],trace[14][step_counter]} <= (cost140 < cost141) ? {cost140,1'b0} : {cost141,1'b1};
				{cost[15],trace[15][step_counter]} <= (cost150 < cost151) ? {cost150,1'b0} : {cost151,1'b1};
				{cost[16],trace[16][step_counter]} <= (cost160 < cost161) ? {cost160,1'b0} : {cost161,1'b1};
				{cost[17],trace[17][step_counter]} <= (cost170 < cost171) ? {cost170,1'b0} : {cost171,1'b1};
				{cost[18],trace[18][step_counter]} <= (cost180 < cost181) ? {cost180,1'b0} : {cost181,1'b1};
				{cost[19],trace[19][step_counter]} <= (cost190 < cost191) ? {cost190,1'b0} : {cost191,1'b1};
				{cost[20],trace[20][step_counter]} <= (cost200 < cost201) ? {cost200,1'b0} : {cost201,1'b1};
				{cost[21],trace[21][step_counter]} <= (cost210 < cost211) ? {cost210,1'b0} : {cost211,1'b1};
				{cost[22],trace[22][step_counter]} <= (cost220 < cost221) ? {cost220,1'b0} : {cost221,1'b1};
				{cost[23],trace[23][step_counter]} <= (cost230 < cost231) ? {cost230,1'b0} : {cost231,1'b1};
				{cost[24],trace[24][step_counter]} <= (cost240 < cost241) ? {cost240,1'b0} : {cost241,1'b1};
				{cost[25],trace[25][step_counter]} <= (cost250 < cost251) ? {cost250,1'b0} : {cost251,1'b1};
				{cost[26],trace[26][step_counter]} <= (cost260 < cost261) ? {cost260,1'b0} : {cost261,1'b1};
				{cost[27],trace[27][step_counter]} <= (cost270 < cost271) ? {cost270,1'b0} : {cost271,1'b1};
				{cost[28],trace[28][step_counter]} <= (cost280 < cost281) ? {cost280,1'b0} : {cost281,1'b1};
				{cost[29],trace[29][step_counter]} <= (cost290 < cost291) ? {cost290,1'b0} : {cost291,1'b1};
				{cost[30],trace[30][step_counter]} <= (cost300 < cost301) ? {cost300,1'b0} : {cost301,1'b1};
				{cost[31],trace[31][step_counter]} <= (cost310 < cost311) ? {cost310,1'b0} : {cost311,1'b1};
				{cost[32],trace[32][step_counter]} <= (cost320 < cost321) ? {cost320,1'b0} : {cost321,1'b1};
				{cost[33],trace[33][step_counter]} <= (cost330 < cost331) ? {cost330,1'b0} : {cost331,1'b1};
				{cost[34],trace[34][step_counter]} <= (cost340 < cost341) ? {cost340,1'b0} : {cost341,1'b1};
				{cost[35],trace[35][step_counter]} <= (cost350 < cost351) ? {cost350,1'b0} : {cost351,1'b1};
				{cost[36],trace[36][step_counter]} <= (cost360 < cost361) ? {cost360,1'b0} : {cost361,1'b1};
				{cost[37],trace[37][step_counter]} <= (cost370 < cost371) ? {cost370,1'b0} : {cost371,1'b1};
				{cost[38],trace[38][step_counter]} <= (cost380 < cost381) ? {cost380,1'b0} : {cost381,1'b1};
				{cost[39],trace[39][step_counter]} <= (cost390 < cost391) ? {cost390,1'b0} : {cost391,1'b1};
				{cost[40],trace[40][step_counter]} <= (cost400 < cost401) ? {cost400,1'b0} : {cost401,1'b1};
				{cost[41],trace[41][step_counter]} <= (cost410 < cost411) ? {cost410,1'b0} : {cost411,1'b1};
				{cost[42],trace[42][step_counter]} <= (cost420 < cost421) ? {cost420,1'b0} : {cost421,1'b1};
				{cost[43],trace[43][step_counter]} <= (cost430 < cost431) ? {cost430,1'b0} : {cost431,1'b1};
				{cost[44],trace[44][step_counter]} <= (cost440 < cost441) ? {cost440,1'b0} : {cost441,1'b1};
				{cost[45],trace[45][step_counter]} <= (cost450 < cost451) ? {cost450,1'b0} : {cost451,1'b1};
				{cost[46],trace[46][step_counter]} <= (cost460 < cost461) ? {cost460,1'b0} : {cost461,1'b1};
				{cost[47],trace[47][step_counter]} <= (cost470 < cost471) ? {cost470,1'b0} : {cost471,1'b1};
				{cost[48],trace[48][step_counter]} <= (cost480 < cost481) ? {cost480,1'b0} : {cost481,1'b1};
				{cost[49],trace[49][step_counter]} <= (cost490 < cost491) ? {cost490,1'b0} : {cost491,1'b1};
				{cost[50],trace[50][step_counter]} <= (cost500 < cost501) ? {cost500,1'b0} : {cost501,1'b1};
				{cost[51],trace[51][step_counter]} <= (cost510 < cost511) ? {cost510,1'b0} : {cost511,1'b1};
				{cost[52],trace[52][step_counter]} <= (cost520 < cost521) ? {cost520,1'b0} : {cost521,1'b1};
				{cost[53],trace[53][step_counter]} <= (cost530 < cost531) ? {cost530,1'b0} : {cost531,1'b1};
				{cost[54],trace[54][step_counter]} <= (cost540 < cost541) ? {cost540,1'b0} : {cost541,1'b1};
				{cost[55],trace[55][step_counter]} <= (cost550 < cost551) ? {cost550,1'b0} : {cost551,1'b1};
				{cost[56],trace[56][step_counter]} <= (cost560 < cost561) ? {cost560,1'b0} : {cost561,1'b1};
				{cost[57],trace[57][step_counter]} <= (cost570 < cost571) ? {cost570,1'b0} : {cost571,1'b1};
				{cost[58],trace[58][step_counter]} <= (cost580 < cost581) ? {cost580,1'b0} : {cost581,1'b1};
				{cost[59],trace[59][step_counter]} <= (cost590 < cost591) ? {cost590,1'b0} : {cost591,1'b1};
				{cost[60],trace[60][step_counter]} <= (cost600 < cost601) ? {cost600,1'b0} : {cost601,1'b1};
				{cost[61],trace[61][step_counter]} <= (cost610 < cost611) ? {cost610,1'b0} : {cost611,1'b1};
				{cost[62],trace[62][step_counter]} <= (cost620 < cost621) ? {cost620,1'b0} : {cost621,1'b1};
				{cost[63],trace[63][step_counter]} <= (cost630 < cost631) ? {cost630,1'b0} : {cost631,1'b1};
			end
		end
	end
	
	wire [4:0] cost00,  cost01,  cost10,  cost11,  cost20,  cost21,  cost30,  cost31,  cost40,  cost41,  cost50,  cost51,  cost60,  cost61,
			   cost70,  cost71,  cost80,  cost81,  cost90,  cost91,  cost100, cost101, cost110, cost111, cost120, cost121, cost130, cost131,
			   cost140, cost141, cost150, cost151, cost160, cost161, cost170, cost171, cost180, cost181, cost190, cost191, cost200, cost201,
			   cost210, cost211, cost220, cost221, cost230, cost231, cost240, cost241, cost250, cost251, cost260, cost261, cost270, cost271,
			   cost280, cost281, cost290, cost291, cost300, cost301, cost310, cost311, cost320, cost321, cost330, cost331, cost340, cost341,
			   cost350, cost351, cost360, cost361, cost370, cost371, cost380, cost381, cost390, cost391, cost400, cost401, cost410, cost411,
			   cost420, cost421, cost430, cost431, cost440, cost441, cost450, cost451, cost460, cost461, cost470, cost471, cost480, cost481,
			   cost490, cost491, cost500, cost501, cost510, cost511, cost520, cost521, cost530, cost531, cost540, cost541, cost550, cost551,
			   cost560, cost561, cost570, cost571, cost580, cost581, cost590, cost591, cost600, cost601, cost610, cost611, cost620, cost621,
			   cost630, cost631;
	
	assign cost00 = cost[0]  + hamming00;
	assign cost01 = cost[32] + hamming11;
	assign cost10 = cost[0]  + hamming11;
	assign cost11 = cost[32] + hamming00;
	
	assign cost20 = cost[1]  + hamming01;
	assign cost21 = cost[33] + hamming10;
	assign cost30 = cost[1]  + hamming10;
	assign cost31 = cost[33] + hamming01;
	
	assign cost40 = cost[2]  + hamming11;
	assign cost41 = cost[34] + hamming00;
	assign cost50 = cost[2]  + hamming00;
	assign cost51 = cost[34] + hamming11;
	
	assign cost60 = cost[3]  + hamming10;
	assign cost61 = cost[35] + hamming01;
	assign cost70 = cost[3]  + hamming01;
	assign cost71 = cost[35] + hamming10;

	assign cost80 = cost[4]  + hamming11;
	assign cost81 = cost[36] + hamming00;
	assign cost90 = cost[4]  + hamming00;
	assign cost91 = cost[36] + hamming11;
	
	assign cost100 = cost[5]  + hamming10;
	assign cost101 = cost[37] + hamming01;
	assign cost110 = cost[5]  + hamming01;
	assign cost111 = cost[37] + hamming10;
	
	assign cost120 = cost[6]  + hamming00;
	assign cost121 = cost[38] + hamming11;
	assign cost130 = cost[6]  + hamming11;
	assign cost131 = cost[38] + hamming00;
	
	assign cost140 = cost[7]  + hamming01;
	assign cost141 = cost[39] + hamming10;
	assign cost150 = cost[7]  + hamming10;
	assign cost151 = cost[39] + hamming01;
	
	assign cost160 = cost[8]  + hamming00;
	assign cost161 = cost[40] + hamming11;
	assign cost170 = cost[8]  + hamming11;
	assign cost171 = cost[40] + hamming00;

	assign cost180 = cost[9]  + hamming01;
	assign cost181 = cost[41] + hamming10;
	assign cost190 = cost[9]  + hamming10;
	assign cost191 = cost[41] + hamming01;

	assign cost200 = cost[10] + hamming11;
	assign cost201 = cost[42] + hamming00;
	assign cost210 = cost[10] + hamming00;
	assign cost211 = cost[42] + hamming11;
	
	assign cost220 = cost[11] + hamming10;
	assign cost221 = cost[43] + hamming01;
	assign cost230 = cost[11] + hamming01;
	assign cost231 = cost[43] + hamming10;
	
	assign cost240 = cost[12] + hamming11;
	assign cost241 = cost[44] + hamming00;
	assign cost250 = cost[12] + hamming00;
	assign cost251 = cost[44] + hamming11;
	
	assign cost260 = cost[13] + hamming10;
	assign cost261 = cost[45] + hamming01;
	assign cost270 = cost[13] + hamming01;
	assign cost271 = cost[45] + hamming10;
	
	assign cost280 = cost[14] + hamming00;
	assign cost281 = cost[46] + hamming11;
	assign cost290 = cost[14] + hamming11;
	assign cost291 = cost[46] + hamming00;
	
	assign cost300 = cost[15] + hamming01;
	assign cost301 = cost[47] + hamming10;
	assign cost310 = cost[15] + hamming10;
	assign cost311 = cost[47] + hamming01;
	
	assign cost320 = cost[16] + hamming10;
	assign cost321 = cost[48] + hamming01;
	assign cost330 = cost[16] + hamming01;
	assign cost331 = cost[48] + hamming10;
	
	assign cost340 = cost[17] + hamming11;
	assign cost341 = cost[49] + hamming00;
	assign cost350 = cost[17] + hamming00;
	assign cost351 = cost[49] + hamming11;
	
	assign cost360 = cost[18] + hamming01;
	assign cost361 = cost[50] + hamming10;
	assign cost370 = cost[18] + hamming10;
	assign cost371 = cost[50] + hamming01;
	
	assign cost380 = cost[19] + hamming00;
	assign cost381 = cost[51] + hamming11;
	assign cost390 = cost[19] + hamming11;
	assign cost391 = cost[51] + hamming00;
	
	assign cost400 = cost[20] + hamming01;
	assign cost401 = cost[52] + hamming10;
	assign cost410 = cost[20] + hamming10;
	assign cost411 = cost[52] + hamming01;
	
	assign cost420 = cost[21] + hamming00;
	assign cost421 = cost[53] + hamming11;
	assign cost430 = cost[21] + hamming11;
	assign cost431 = cost[53] + hamming00;
	
	assign cost440 = cost[22] + hamming10;
	assign cost441 = cost[54] + hamming01;
	assign cost450 = cost[22] + hamming01;
	assign cost451 = cost[54] + hamming10;
	
	assign cost460 = cost[23] + hamming11;
	assign cost461 = cost[55] + hamming00;
	assign cost470 = cost[23] + hamming00;
	assign cost471 = cost[55] + hamming11;
	
	assign cost480 = cost[24] + hamming10;
	assign cost481 = cost[56] + hamming01;
	assign cost490 = cost[24] + hamming01;
	assign cost491 = cost[56] + hamming10;
	
	assign cost500 = cost[25] + hamming11;
	assign cost501 = cost[57] + hamming00;
	assign cost510 = cost[25] + hamming00;
	assign cost511 = cost[57] + hamming11;
	
	assign cost520 = cost[26] + hamming01;
	assign cost521 = cost[58] + hamming10;
	assign cost530 = cost[26] + hamming10;
	assign cost531 = cost[58] + hamming01;
	
	assign cost540 = cost[27] + hamming00;
	assign cost541 = cost[59] + hamming11;
	assign cost550 = cost[27] + hamming11;
	assign cost551 = cost[59] + hamming00;
	
	assign cost560 = cost[28] + hamming01;
	assign cost561 = cost[60] + hamming10;
	assign cost570 = cost[28] + hamming10;
	assign cost571 = cost[60] + hamming01;
	
	assign cost580 = cost[29] + hamming00;
	assign cost581 = cost[61] + hamming11;
	assign cost590 = cost[29] + hamming11;
	assign cost591 = cost[61] + hamming00;
	
	assign cost600 = cost[30] + hamming10;
	assign cost601 = cost[62] + hamming01;
	assign cost610 = cost[30] + hamming01;
	assign cost611 = cost[62] + hamming10;
	
	assign cost620 = cost[31] + hamming11;
	assign cost621 = cost[63] + hamming00;
	assign cost630 = cost[31] + hamming00;
	assign cost631 = cost[63] + hamming11;

endmodule
module comparator64(
	input clk,
	input start, // should be true just for one clock
	input resetn,
	input [4:0] in0,
	input [4:0] in1,
	input [4:0] in2,
	input [4:0] in3,
	input [4:0] in4,
	input [4:0] in5,
	input [4:0] in6,
	input [4:0] in7,
	input [4:0] in8,
	input [4:0] in9,
	input [4:0] in10,
	input [4:0] in11,
	input [4:0] in12,
	input [4:0] in13,
	input [4:0] in14,
	input [4:0] in15,
	input [4:0] in16,
	input [4:0] in17,
	input [4:0] in18,
	input [4:0] in19,
	input [4:0] in20,
	input [4:0] in21,
	input [4:0] in22,
	input [4:0] in23,
	input [4:0] in24,
	input [4:0] in25,
	input [4:0] in26,
	input [4:0] in27,
	input [4:0] in28,
	input [4:0] in29,
	input [4:0] in30,
	input [4:0] in31,
	input [4:0] in32,
	input [4:0] in33,
	input [4:0] in34,
	input [4:0] in35,
	input [4:0] in36,
	input [4:0] in37,
	input [4:0] in38,
	input [4:0] in39,
	input [4:0] in40,
	input [4:0] in41,
	input [4:0] in42,
	input [4:0] in43,
	input [4:0] in44,
	input [4:0] in45,
	input [4:0] in46,
	input [4:0] in47,
	input [4:0] in48,
	input [4:0] in49,
	input [4:0] in50,
	input [4:0] in51,
	input [4:0] in52,
	input [4:0] in53,
	input [4:0] in54,
	input [4:0] in55,
	input [4:0] in56,
	input [4:0] in57,
	input [4:0] in58,
	input [4:0] in59,
	input [4:0] in60,
	input [4:0] in61,
	input [4:0] in62,
	input [4:0] in63,
	output reg done,
	output [5:0] num_res0
	);
	
	reg [4:0] temp0, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, temp10, temp11, temp12, 
		temp13, temp14, temp15, temp16, temp17, temp18, temp19, temp20, temp21, temp22, temp23, temp24, 
		temp25, temp26, temp27, temp28, temp29, temp30, temp31, temp32, temp33, temp34, temp35, temp36, 
		temp37, temp38, temp39, temp40, temp41, temp42, temp43, temp44, temp45, temp46, temp47, temp48, 
		temp49, temp50, temp51, temp52, temp53, temp54, temp55, temp56, temp57, temp58, temp59, temp60, 
		temp61, temp62, temp63;
	reg [5:0] num0, num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, 
			  num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, 
		   	  num25, num26, num27, num28, num29, num30, num31, num32, num33, num34, num35, num36, 
			  num37, num38, num39, num40, num41, num42, num43, num44, num45, num46, num47, num48, 
			  num49, num50, num51, num52, num53, num54, num55, num56, num57, num58, num59, num60, 
			  num61, num62, num63;
	wire [4:0] result0, result1, result2, result3, result4, result5, result6, result7, result8, result9, result10, result11, result12, 
		result13, result14, result15, result16, result17, result18, result19, result20, result21, result22, result23, result24, 
		result25, result26, result27, result28, result29, result30, result31;
	wire [5:0] num_res1, num_res2, num_res3, num_res4, num_res5, num_res6, num_res7, num_res8, num_res9, num_res10, num_res11, num_res12, 
		num_res13, num_res14, num_res15, num_res16, num_res17, num_res18, num_res19, num_res20, num_res21, num_res22, num_res23, num_res24, 
		num_res25, num_res26, num_res27, num_res28, num_res29, num_res30, num_res31;
	reg [2:0] counter;
	
	assign {result0, num_res0} = (temp1 < temp0) ? {temp1, num1} : {temp0, num0};
	assign {result1, num_res1} = (temp3 < temp2) ? {temp3, num3} : {temp2, num2};
	assign {result2, num_res2} = (temp5 < temp4) ? {temp5, num5} : {temp4, num4};
	assign {result3, num_res3} = (temp7 < temp6) ? {temp7, num7} : {temp6, num6};
	assign {result4, num_res4} = (temp9 < temp8) ? {temp9, num9} : {temp8, num8};
	assign {result5, num_res5} = (temp11 < temp10) ? {temp11, num11} : {temp10, num10};
	assign {result6, num_res6} = (temp13 < temp12) ? {temp13, num13} : {temp12, num12};
	assign {result7, num_res7} = (temp15 < temp14) ? {temp15, num15} : {temp14, num14};
	assign {result8, num_res8} = (temp17 < temp16) ? {temp17, num17} : {temp16, num16};
	assign {result9, num_res9} = (temp19 < temp18) ? {temp19, num19} : {temp18, num18};
	assign {result10, num_res10} = (temp21 < temp20) ? {temp21, num21} : {temp20, num20};
	assign {result11, num_res11} = (temp23 < temp22) ? {temp23, num23} : {temp22, num22};
	assign {result12, num_res12} = (temp25 < temp24) ? {temp25, num25} : {temp24, num24};
	assign {result13, num_res13} = (temp27 < temp26) ? {temp27, num27} : {temp26, num26};
	assign {result14, num_res14} = (temp29 < temp28) ? {temp29, num29} : {temp28, num28};
	assign {result15, num_res15} = (temp31 < temp30) ? {temp31, num31} : {temp30, num30};
	assign {result16, num_res16} = (temp33 < temp32) ? {temp33, num33} : {temp32, num32};
	assign {result17, num_res17} = (temp35 < temp34) ? {temp35, num35} : {temp34, num34};
	assign {result18, num_res18} = (temp37 < temp36) ? {temp37, num37} : {temp36, num36};
	assign {result19, num_res19} = (temp39 < temp38) ? {temp39, num39} : {temp38, num38};
	assign {result20, num_res20} = (temp41 < temp40) ? {temp41, num41} : {temp40, num40};
	assign {result21, num_res21} = (temp43 < temp42) ? {temp43, num43} : {temp42, num42};
	assign {result22, num_res22} = (temp45 < temp44) ? {temp45, num45} : {temp44, num44};
	assign {result23, num_res23} = (temp47 < temp46) ? {temp47, num47} : {temp46, num46};
	assign {result24, num_res24} = (temp49 < temp48) ? {temp49, num49} : {temp48, num48};
	assign {result25, num_res25} = (temp51 < temp50) ? {temp51, num51} : {temp50, num50};
	assign {result26, num_res26} = (temp53 < temp52) ? {temp53, num53} : {temp52, num52};
	assign {result27, num_res27} = (temp55 < temp54) ? {temp55, num55} : {temp54, num54};
	assign {result28, num_res28} = (temp57 < temp56) ? {temp57, num57} : {temp56, num56};
	assign {result29, num_res29} = (temp59 < temp58) ? {temp59, num59} : {temp58, num58};
	assign {result30, num_res30} = (temp61 < temp60) ? {temp61, num61} : {temp60, num60};
	assign {result31, num_res31} = (temp63 < temp62) ? {temp63, num63} : {temp62, num62};
	
	always @(posedge clk) begin
		if (~resetn) begin
			counter <= 3'd0;
			done <= 1'b0;
			num0 <= 6'd0;
			num1 <= 6'd1;
			num2 <= 6'd2;
			num3 <= 6'd3;
			num4 <= 6'd4;
			num5 <= 6'd5;
			num6 <= 6'd6;
			num7 <= 6'd7;
			num8 <= 6'd8;
			num9 <= 6'd9;
			num10 <= 6'd10;
			num11 <= 6'd11;
			num12 <= 6'd12;
			num13 <= 6'd13;
			num14 <= 6'd14;
			num15 <= 6'd15;
			num16 <= 6'd16;
			num17 <= 6'd17;
			num18 <= 6'd18;
			num19 <= 6'd19;
			num20 <= 6'd20;
			num21 <= 6'd21;
			num22 <= 6'd22;
			num23 <= 6'd23;
			num24 <= 6'd24;
			num25 <= 6'd25;
			num26 <= 6'd26;
			num27 <= 6'd27;
			num28 <= 6'd28;
			num29 <= 6'd29;
			num30 <= 6'd30;
			num31 <= 6'd31;
			num32 <= 6'd32;
			num33 <= 6'd33;
			num34 <= 6'd34;
			num35 <= 6'd35;
			num36 <= 6'd36;
			num37 <= 6'd37;
			num38 <= 6'd38;
			num39 <= 6'd39;
			num40 <= 6'd40;
			num41 <= 6'd41;
			num42 <= 6'd42;
			num43 <= 6'd43;
			num44 <= 6'd44;
			num45 <= 6'd45;
			num46 <= 6'd46;
			num47 <= 6'd47;
			num48 <= 6'd48;
			num49 <= 6'd49;
			num50 <= 6'd50;
			num51 <= 6'd51;
			num52 <= 6'd52;
			num53 <= 6'd53;
			num54 <= 6'd54;
			num55 <= 6'd55;
			num56 <= 6'd56;
			num57 <= 6'd57;
			num58 <= 6'd58;
			num59 <= 6'd59;
			num60 <= 6'd60;
			num61 <= 6'd61;
			num62 <= 6'd62;
			num63 <= 6'd63;
		end
		
		else if (start) begin
			temp0 <= in0;
			temp1 <= in1;
			temp2 <= in2;
			temp3 <= in3;
			temp4 <= in4;
			temp5 <= in5;
			temp6 <= in6;
			temp7 <= in7;
			temp8 <= in8;
			temp9 <= in9;
			temp10 <= in10;
			temp11 <= in11;
			temp12 <= in12;
			temp13 <= in13;
			temp14 <= in14;
			temp15 <= in15;
			temp16 <= in16;
			temp17 <= in17;
			temp18 <= in18;
			temp19 <= in19;
			temp20 <= in20;
			temp21 <= in21;
			temp22 <= in22;
			temp23 <= in23;
			temp24 <= in24;
			temp25 <= in25;
			temp26 <= in26;
			temp27 <= in27;
			temp28 <= in28;
			temp29 <= in29;
			temp30 <= in30;
			temp31 <= in31;
			temp32 <= in32;
			temp33 <= in33;
			temp34 <= in34;
			temp35 <= in35;
			temp36 <= in36;
			temp37 <= in37;
			temp38 <= in38;
			temp39 <= in39;
			temp40 <= in40;
			temp41 <= in41;
			temp42 <= in42;
			temp43 <= in43;
			temp44 <= in44;
			temp45 <= in45;
			temp46 <= in46;
			temp47 <= in47;
			temp48 <= in48;
			temp49 <= in49;
			temp50 <= in50;
			temp51 <= in51;
			temp52 <= in52;
			temp53 <= in53;
			temp54 <= in54;
			temp55 <= in55;
			temp56 <= in56;
			temp57 <= in57;
			temp58 <= in58;
			temp59 <= in59;
			temp60 <= in60;
			temp61 <= in61;
			temp62 <= in62;
			temp63 <= in63;
			counter <= 3'd1;
		end
		
		else if (counter == 3'd1) begin
			{temp0, num0} <= {result0, num_res0};
			{temp1, num1} <= {result1, num_res1};
			{temp2, num2} <= {result2, num_res2};
			{temp3, num3} <= {result3, num_res3};
			{temp4, num4} <= {result4, num_res4};
			{temp5, num5} <= {result5, num_res5};
			{temp6, num6} <= {result6, num_res6};
			{temp7, num7} <= {result7, num_res7};
			{temp8, num8} <= {result8, num_res8};
			{temp9, num9} <= {result9, num_res9};
			{temp10, num10} <= {result10, num_res10};
			{temp11, num11} <= {result11, num_res11};
			{temp12, num12} <= {result12, num_res12};
			{temp13, num13} <= {result13, num_res13};
			{temp14, num14} <= {result14, num_res14};
			{temp15, num15} <= {result15, num_res15};
			{temp16, num16} <= {result16, num_res16};
			{temp17, num17} <= {result17, num_res17};
			{temp18, num18} <= {result18, num_res18};
			{temp19, num19} <= {result19, num_res19};
			{temp20, num20} <= {result20, num_res20};
			{temp21, num21} <= {result21, num_res21};
			{temp22, num22} <= {result22, num_res22};
			{temp23, num23} <= {result23, num_res23};
			{temp24, num24} <= {result24, num_res24};
			{temp25, num25} <= {result25, num_res25};
			{temp26, num26} <= {result26, num_res26};
			{temp27, num27} <= {result27, num_res27};
			{temp28, num28} <= {result28, num_res28};
			{temp29, num29} <= {result29, num_res29};
			{temp30, num30} <= {result30, num_res30};
			{temp31, num31} <= {result31, num_res31};
			counter <= 3'd2;
		end
		
		else if (counter == 3'd2) begin
			{temp0, num0} <= {result0, num_res0};
			{temp1, num1} <= {result1, num_res1};
			{temp2, num2} <= {result2, num_res2};
			{temp3, num3} <= {result3, num_res3};
			{temp4, num4} <= {result4, num_res4};
			{temp5, num5} <= {result5, num_res5};
			{temp6, num6} <= {result6, num_res6};
			{temp7, num7} <= {result7, num_res7};
			{temp8, num8} <= {result8, num_res8};
			{temp9, num9} <= {result9, num_res9};
			{temp10, num10} <= {result10, num_res10};
			{temp11, num11} <= {result11, num_res11};
			{temp12, num12} <= {result12, num_res12};
			{temp13, num13} <= {result13, num_res13};
			{temp14, num14} <= {result14, num_res14};
			{temp15, num15} <= {result15, num_res15};
			counter <= 3'd3;
		end
		
		else if (counter == 3'd3) begin
			{temp0, num0} <= {result0, num_res0};
			{temp1, num1} <= {result1, num_res1};
			{temp2, num2} <= {result2, num_res2};
			{temp3, num3} <= {result3, num_res3};
			{temp4, num4} <= {result4, num_res4};
			{temp5, num5} <= {result5, num_res5};
			{temp6, num6} <= {result6, num_res6};
			{temp7, num7} <= {result7, num_res7};
			counter <= 3'd4;
		end
		
		else if (counter == 3'd4) begin
			{temp0, num0} <= {result0, num_res0};
			{temp1, num1} <= {result1, num_res1};
			{temp2, num2} <= {result2, num_res2};
			{temp3, num3} <= {result3, num_res3};
			counter <= 3'd5;
		end
		
		else if (counter == 3'd5) begin
			{temp0, num0} <= {result0, num_res0};
			{temp1, num1} <= {result1, num_res1};
			done <= 1'b1;
		end
		
		if (done) begin
			done <= 1'b0;
			counter <= 3'd0;
			num0 <= 6'd0;
			num1 <= 6'd1;
			num2 <= 6'd2;
			num3 <= 6'd3;
			num4 <= 6'd4;
			num5 <= 6'd5;
			num6 <= 6'd6;
			num7 <= 6'd7;
			num8 <= 6'd8;
			num9 <= 6'd9;
			num10 <= 6'd10;
			num11 <= 6'd11;
			num12 <= 6'd12;
			num13 <= 6'd13;
			num14 <= 6'd14;
			num15 <= 6'd15;
			num16 <= 6'd16;
			num17 <= 6'd17;
			num18 <= 6'd18;
			num19 <= 6'd19;
			num20 <= 6'd20;
			num21 <= 6'd21;
			num22 <= 6'd22;
			num23 <= 6'd23;
			num24 <= 6'd24;
			num25 <= 6'd25;
			num26 <= 6'd26;
			num27 <= 6'd27;
			num28 <= 6'd28;
			num29 <= 6'd29;
			num30 <= 6'd30;
			num31 <= 6'd31;
			num32 <= 6'd32;
			num33 <= 6'd33;
			num34 <= 6'd34;
			num35 <= 6'd35;
			num36 <= 6'd36;
			num37 <= 6'd37;
			num38 <= 6'd38;
			num39 <= 6'd39;
			num40 <= 6'd40;
			num41 <= 6'd41;
			num42 <= 6'd42;
			num43 <= 6'd43;
			num44 <= 6'd44;
			num45 <= 6'd45;
			num46 <= 6'd46;
			num47 <= 6'd47;
			num48 <= 6'd48;
			num49 <= 6'd49;
			num50 <= 6'd50;
			num51 <= 6'd51;
			num52 <= 6'd52;
			num53 <= 6'd53;
			num54 <= 6'd54;
			num55 <= 6'd55;
			num56 <= 6'd56;
			num57 <= 6'd57;
			num58 <= 6'd58;
			num59 <= 6'd59;
			num60 <= 6'd60;
			num61 <= 6'd61;
			num62 <= 6'd62;
			num63 <= 6'd63;
		end
	end
endmodule
