`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   08:55:27 07/01/2020
// Design Name:   decoder
// Module Name:   C:/.Xilinx/decoder/decoder_tb.v
// Project Name:  decoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: decoder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module decoder_tb;

	// Inputs
	reg [5:0] in;
	reg clk;
	reg rate;

	// Outputs
	wire [2:0]out;
	reg [2:0]temp_res;
	reg [9:0]failures=0;

	// Instantiate the Unit Under Test (UUT)
	decoder uut (
		.in(in), 
		.clk(clk), 
		.rate(rate), 
		.out(out)
	);
integer op1,temp1,op2,temp2;
	initial begin
		// Initialize Inputs
		in = 0;
		clk = 0;
		rate = 1;
op1=$fopen("out.txt","r");  
op2=$fopen("in.txt","r");  
temp1= $fscanf (op1, "%b \n", in[5]); 
temp1= $fscanf (op1, "%b \n", in[4]); 
temp1= $fscanf (op1, "%b \n", in[3]); 
temp1= $fscanf (op1, "%b \n", in[2]); 
temp1= $fscanf (op1, "%b \n", in[1]); 
temp1= $fscanf (op1, "%b \n", in[0]); 
		// Wait 100 ns for global reset to finish
        		// Add stimulus here

	end
	always  #5 clk<= ~clk ;
				always@(posedge clk) 
					begin
      temp1= $fscanf (op1, "%b \n", in[5]); 
temp1= $fscanf (op1, "%b \n", in[4]); 
temp1= $fscanf (op1, "%b \n", in[3]); 
temp1= $fscanf (op1, "%b \n", in[2]); 
temp1= $fscanf (op1, "%b \n", in[1]); 
temp1= $fscanf (op1, "%b \n", in[0]); 
temp2= $fscanf (op2, "%b \n", temp_res[2]); 
temp2= $fscanf (op2, "%b \n", temp_res[1]); 
temp2= $fscanf (op2, "%b \n", temp_res[0]); 
#2;
if(!$feof(op2))
if(temp_res!=out)
failures=failures+1;
		end
endmodule

