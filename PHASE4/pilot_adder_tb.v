`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:47:14 07/24/2020
// Design Name:   pilot_adder
// Module Name:   C:/.Xilinx/pilot_adder/pilot_adder_tb.v
// Project Name:  pilot_adder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pilot_adder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pilot_adder_tb;

	// Inputs
	reg [95:0] in;
	reg polarity;
	reg imORreal;
	reg clk;

	// Outputs
	wire [105:0] out;

	// Instantiate the Unit Under Test (UUT)
	pilot_adder uut (
		.in(in), 
		.polarity(polarity), 
		.imORreal(imORreal), 
		.clk(clk), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		polarity = 1;
		imORreal = 1;
		clk = 0;

		// Wait 100 ns for global reset to finish
		
        
		// Add stimulus here

	end
	always #5 clk=~clk;
      
endmodule

