module mapper(
input wire [1:0] in,
input wire clk,
input wire modulation,
output reg [1:0] I,
output reg [1:0] Q
);

always@(posedge clk)
begin
if(modulation == 0)
begin
Q=2'b00;
if(in[0]==0)
I=2'b11;
else
I=2'b01;
end
else
begin
if(in[0]==0)
I=2'b11;
else
I=2'b01;
if(in[1]==0)
Q=2'b11;
else
Q=2'b01;
end
end
endmodule
