module pilot_remover(input wire [105:0]in,input wire clk,output reg [95:0]out);
always@(posedge clk)
begin
out[9:0]=in[9:0];
out[35:10]=in[37:12];
out[47:36]=in[51:40];
out[59:48]=in[65:54];
out[85:60]=in[93:68];
out[95:86]=in[105:96];
end
endmodule
