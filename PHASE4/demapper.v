module demapper
(
input wire [1:0]I,
input wire[1:0]Q,
input wire clk,
input wire modulation,
output reg[1:0]out
    );
always@(posedge clk)
begin
if(modulation==0)
begin
out[1]=0;
if(I==2'b11)
out[0]=0;
else
out[0]=1;
end
else
begin
if(I==2'b11)
out[0]=0;
else
out[0]=1;
if(Q==2'b11)
out[1]=0;
else
out[1]=1;
end
end
endmodule
