`timescale 1ns / 1ps
module pilot_remover_tb;
	reg [105:0] in;
	reg clk;
	wire [95:0] out;
	pilot_remover uut (
		.in(in), 
		.clk(clk), 
		.out(out)
	);

	initial 
	begin
		in = 0;
		clk = 0;
	end
	always #5 clk=~clk;
	always@(posedge clk)
	begin
	in=$random;
	end
      
endmodule

