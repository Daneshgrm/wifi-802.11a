`timescale 1ns / 1ps
module demapper_tb;
	reg [1:0] I;
	reg [1:0] Q;
	reg clk;
	reg modulation;
	wire [1:0] out;
	demapper uut (
		.I(I), 
		.Q(Q), 
		.clk(clk), 
		.modulation(modulation), 
		.out(out)
	);
	initial begin
		I = 0;
		Q = 0;
		clk = 0;
		modulation = 1;
	end
	always #5 clk=~clk;
	always@(posedge clk)
	begin
	#1;
	Q[1]=$random;
	Q[0]=1;
	I[1]=$random;
	I[0]=1;
	
	end
      
endmodule

