`timescale 1ns / 1ps
module mapper_tb;
	reg [1:0] in;
	reg clk;
	reg modulation;
	wire [1:0] I;
	wire [1:0] Q;
	mapper uut (
		.in(in), 
		.clk(clk), 
		.modulation(modulation), 
		.I(I), 
		.Q(Q)
	);
	initial begin
		in = 0;
		clk = 0;
		modulation = 1;
	end
	always #5 clk=~clk;
	always@(posedge clk)
	begin
	#1 in[0]=$random;
	#1 in[1]=$random;
	end
      
endmodule

