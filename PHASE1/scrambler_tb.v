`timescale 1ns / 1ps

module scramble_tb;
	reg  in;
	reg clk;
	wire  out;
	reg temp_res;
	reg[7:0]failures=0;

	de_scramble uut (
		.in(in), 
		.clk(clk), 
		.out(out) 


	);
  integer op1,temp1,op2,temp2;

	initial 
	begin
		// Initialize Inputs
		op1=$fopen("in.txt","r");  
		op2=$fopen("out.txt","r"); 
		temp1= $fscanf (op2, "%b \n", in); 
		clk = 0;
		in=0;
		 
			
	end
			always  #2 clk<= ~clk ;
				always@(posedge clk) 
					begin
      temp1= $fscanf (op2, "%b \n", in);
		temp2= $fscanf (op1, "%b \n", temp_res);
				#1;
				if(!$feof(op2))
				begin
		if(temp_res!=out)
		failures=failures+1;
		end
		$display("failures = %b",failures);
		  end
		  
endmodule
