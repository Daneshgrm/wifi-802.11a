LENGTH = input('Please enter LENGTH: ');
DATARATE = input('Please enter DATARATE: ');
PSDU = input('Please enter PSDU: ');
% LENGTH = 3;
% DATARATE = 6;
% PSDU = '11101110_11110111_11101111';
PSDU = bin2dec(cell2mat(split(PSDU, '_')'));

SERVICE = fi(0, 0, 16, 0);
TXVECTOR = PHY_TXSTART_REQUEST(LENGTH, DATARATE, SERVICE, 1);
RATE_PARAMS = SET_RATE_PARAMS(TXVECTOR);

N_SYM = ceil((16 + 8*TXVECTOR.LENGTH+6)/RATE_PARAMS.NDBPS);
N_DATA = N_SYM * RATE_PARAMS.NDBPS;
N_PAD = N_DATA - (16 + 8 * TXVECTOR.LENGTH + 6);
TAIL_PAD = fi(0, 0, 6 + N_PAD, 0);
PSDU = fi(PSDU, 0, 8*LENGTH, 0);
DATA = bitconcat(SERVICE, PSDU, TAIL_PAD);
input_data = bin(DATA)';
inputFileID = fopen('in.txt', 'w');
fprintf(inputFileID, '%c \n', input_data(:));
SCRAMBLED_DATA = SCRAMBLE(DATA);
output_data = bin(SCRAMBLED_DATA)';
inputFileID = fopen('out.txt', 'w');
fprintf(inputFileID, '%c \n', output_data(:));

function SCRAMBLED_DATA = SCRAMBLE(DATA)
    seed = fi(127, 0, 7, 0);
    SCRAMBLED_DATA = fi(0, 0, DATA.wordLength, 0);
    for i = 1:DATA.wordLength
        index = DATA.wordLength+1-i;
        feedback = xor(bitget(seed, 7), bitget(seed, 4));
        seed = bitsll(seed, 1);
        seed = bitset(seed, 1, feedback);
        SCRAMBLED_DATA =  bitset(SCRAMBLED_DATA, index, xor(feedback, bitget(DATA, index)));
    end
end
function RATE_PARAMS = SET_RATE_PARAMS(TXVECTOR)
    rate_tmp = TXVECTOR.DATARATE;
    switch(rate_tmp)
        case 6
            nbpsc = 1;
            ncbps = 48;
            ndbps = 24;
        case 12
            nbpsc = 2;
            ncbps = 96;
            ndbps = 48;
        case 24
            nbpsc = 4;
            ncbps = 192;
            ndbps = 96;
        otherwise
            disp('Error - RATE');
    end
    RATE_PARAMS = struct('NBPSC', nbpsc, 'NCBPS', ncbps, 'NDBPS', ndbps);

end
function TXVECTOR = PHY_TXSTART_REQUEST(LENGTH, DATARATE, SERVICE, TXPWR_LEVEL)
    
        TXVECTOR = struct('LENGTH', LENGTH, ...
                      'DATARATE', DATARATE, ...
                      'SERVICE', SERVICE, ...
                      'TXPWR_LEVEL', TXPWR_LEVEL);
end
