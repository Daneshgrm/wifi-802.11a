`timescale 1ns / 1ps
module de_scramble(in,clk,out);
input wire in;
input wire clk;
output reg out;
reg [7:1]seed=7'b1111111;
reg temp;
always@(posedge clk)
begin
out=in^seed[7]^seed[4];
temp=seed[7]^seed[4];
seed=seed<<1;
seed[1]=temp;
end
endmodule
