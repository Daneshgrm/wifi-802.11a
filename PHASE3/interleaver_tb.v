`timescale 1ns / 1ps
module interleaver_tb;
	reg [95:0] in;
	reg N;
	reg clk;
	wire [95:0] out;
reg failures=0;
reg [95:0]temp_res;
	interleaver uut (
		.in(in), 
		.N(N), 
		.clk(clk), 
		.out(out)
	);
  integer op1,temp,counter,op2;
	initial begin
		in = 0;
		N = 0;
		clk = 0;
op1=$fopen("in.txt","r"); 
op2=$fopen("out.txt","r"); 
	end
	always  #5 clk<= ~clk ;
	always@(posedge clk)
	begin
	for(counter=0;counter<=95;counter=counter+1)
	begin
	temp= $fscanf (op1, "%b \n",in[counter]);
	end
	for(counter=0;counter<=95;counter=counter+1)
	begin
	temp= $fscanf (op2, "%b \n",temp_res[counter]);
	end
	#12;
if(N==0 && out[47:0]!= temp_res[47:0])
failures=1;
if(N==1 && out!= temp_res)
failures=1;
	end    
endmodule

