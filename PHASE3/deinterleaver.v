module deinterleaver(
input wire [95:0]in,
input wire N,
input wire clk,
output reg [95:0]out
    );
	 integer counter;
	 always@(posedge clk)
	 begin
	 if(N==0)
	 begin
	 for(counter=0;counter<=47;counter=counter+1)
	 begin
	 out[16*(counter%3)+(counter/3)]=in[counter];
	 end
	 out[95:48]=0;
	 end
	 if(N==1)
	 begin
	  for(counter=0;counter<=95;counter=counter+1)
	 begin
	 out[16*(counter%6)+(counter/6)]=in[counter];
	 end
	 end
	 end
endmodule
