module interleaver(
input wire [95:0]in,
input wire N,
input wire clk,
output reg [95:0]out
    );
	 integer counter;
	 always@(posedge clk)
	 begin
	 if(N==0)
	 begin
	 for(counter=0;counter<=47;counter=counter+1)
	 begin
	 out[3*(counter%16) +(counter/16)]=in[counter];
	 end
	 out[95:48]=0;
	 end
	 if(N==1)
	 begin
	  for(counter=0;counter<=95;counter=counter+1)
	 begin
	 out[6*(counter%16) + (counter/16)]=in[counter];
	 end
	 end
	 end
endmodule
